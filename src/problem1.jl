### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 3e2b34f6-d986-11eb-0d86-9b1ec2a4ec1e
begin
	using Pkg; Pkg.activate("../Project.toml")
	using PlutoUI
	
	with_terminal() do
		Pkg.status()
	end
end

# ╔═╡ 98199c47-1b53-48ac-8e23-073740326b87
md"# Problem 1"

# ╔═╡ 07020a9e-ba33-4cb4-aaa3-baa138e610f3
md"## Capture a MDP"

# ╔═╡ d05e59bc-f62b-422a-8c6f-9381d54acecc
md"Enter number of states $(@bind nof_states NumberField(1:10))"

# ╔═╡ 37925a22-d774-4076-8ee7-a3f88f8b6f88
md"Enter number of actions $(@bind nof_actions NumberField(1:10))"

# ╔═╡ ac9184d4-ebc2-4af2-b91c-ed106dbae337
R = zeros(nof_states, nof_states, nof_actions)

# ╔═╡ 1101e7bf-0cfd-458b-a40b-7ff77cc10b50
P = zeros(nof_states, nof_states, nof_actions)

# ╔═╡ 412d34c2-a77d-49ad-87cb-64e4c039b127
md"## Transition model and rewards"

# ╔═╡ e13c682f-a38d-48fc-89a0-232eb872dce6
md"Define transition and reward matrices for action $(@bind a NumberField(1:nof_actions))"

# ╔═╡ c54b6eae-ec48-4746-b733-86995ee10eb7
md"P[:, :, $a ] $(@bind s1 TextField())

R[:, :, $a ] $(@bind s2 TextField()) 

$(@bind update Button(\"Update\"))" 

# ╔═╡ e02659ec-18d9-44c0-a5e8-687f5b9c0775
md" Transition model P"

# ╔═╡ 7b284aef-54d2-48de-93a7-b4fd0493a3a0
begin
	update
	
	P[:,:,a]
end

# ╔═╡ 603c82fc-4a16-48a5-8609-9968e9554baa
md"Reward per transition R"

# ╔═╡ b2c025ab-840e-4407-8fe9-4d93ff13f12a
begin
	update
	
	R[:,:,a]
end

# ╔═╡ 377a59f4-67bb-4e79-bc9a-6bf2814dd8d2
begin
	if s1 != ""
		rows = split(s1, ";")
		for j in 1:length(rows)
			numbers = split(rows[j], " ")
			filter!(n -> n != "", numbers)
			for i in 1:length(numbers)
				P[i,j,a] = parse(Float64, numbers[i])
			end
		end
	end
	
	if s2 != ""
		rows = split(s2, ";")
		for j in 1:length(rows)
			numbers = split(rows[j], " ")
			filter!(n -> n != "", numbers)
			for i in 1:length(numbers)
				R[i,j,a] = parse(Float64, numbers[i])
			end
		end
	end
end

# ╔═╡ bad25383-8359-4cc8-b6d4-5656acc62d4d
md"### Example game"

# ╔═╡ 80ac8292-9e62-4a0b-a639-504da3a33c34
md"Use example game? $(@bind enable_example CheckBox())"

# ╔═╡ 0cabddc5-74a9-4c56-af81-b11c4a09b4f2
if enable_example	
	r = -0.01
	
	P[:,:,1] = [.1 .0 .0 .0 .0 .0;
				.1 .0 .0 .0 .0 .0;
				.0 .0 .0 .0 .0 .0;
				.8 .0 .0 .9 .1 .0;
				.0 .0 .0 .1 .8 .1;
				.0 .0 .0 .0 .1 .9;]
	
	P[:,:,2] = [.1 .0 .0 .1 .0 .0;
				.8 .0 .0 .0 .1 .0;
				.0 .0 .0 .0 .0 .1;
				.1 .0 .0 .1 .0 .0;
				.0 .0 .0 .8 .1 .0;
		     	.0 .0 .0 .0 .8 .9;]
	
	P[:,:,3] = [.9 .0 .0 .8 .0 .0;
				.1 .0 .0 .0 .8 .0;
				.0 .0 .0 .0 .0 .8;
				.0 .0 .0 .1 .1 .0;
				.0 .0 .0 .1 .0 .1;
				.0 .0 .0 .0 .1 .1;]
	
	P[:,:,4] = [.9 .0 .0 .1 .0 .0;
				.0 .0 .0 .0 .1 .0;
				.0 .0 .0 .0 .0 .1;
				.1 .0 .0 .9 .8 .0;
				.0 .0 .0 .0 .1 .8;
				.0 .0 .0 .0 .0 .1;]

	
	R[:,:,1] = [ r .0 .0 .0 .0 .0;
			    -1 .0 .0 .0 .0 .0;
			    .0 .0 .0 .0 .0 .0;
			     r .0 .0  r  r .0;
				.0 .0 .0  r  r  r;
				.0 .0 .0 .0  r  r;]
	
	R[:,:,2] = [ r .0 .0  r .0 .0;
			    -1 .0 .0 .0 -1 .0;
			    .0 .0 .0 .0 .0  1;
			     r .0 .0  r .0 .0;
			    .0 .0 .0  r  r .0;
			    .0 .0 .0 .0  r  r;]
	
	R[:,:,3] = [ r .0 .0  r .0 .0;
			    -1 .0 .0 .0 -1 .0;
			    .0 .0 .0 .0 .0 .1;
			    .0 .0 .0  r  r .0;
			    .0 .0 .0  r .0  r;
			    .0 .0 .0 .0  r  r;]
	
	R[:,:,4] = [ r .0 .0  r .0 .0;
			    .0 .0 .0 .0 -1 .0;
			    .0 .0 .0 .0 .0  1;
			     r .0 .0  r  r .0;
			    .0 .0 .0 .0  r  r;
			    .0 .0 .0 .0 .0  r;]
end

# ╔═╡ 3d9b410c-e359-4c94-9822-72374f74752e
md"## Get a solution"

# ╔═╡ 614f978c-599a-4306-8e9c-2305fa17da43
gamma = 0.99

# ╔═╡ b7a410bb-0261-4ff3-bb08-8f99e17e2871
function Qvalue(P, R, s, a, U)
	sum = 0
	for i in 1:nof_states
		sum += P[s,i,a] * (R[s,i,a] + gamma * U[i])
	end
	return sum
end

# ╔═╡ 1fa56533-541b-450e-b336-223c18df71e3
function policy_evaluation(pi, U, P, R)
	U_next = U
	sigma = 1.0
	epsilon = eps(Float32)
	while sigma > epsilon
		for s in 1:nof_states
			U_next[s] = sum(d -> P[s,d,pi[s]] * (R[s,d,pi[s]] + gamma * U[d]), 1:nof_states)
		end
		sigma = sum(map(x -> abs(x), U - U_next))
		U = U_next
	end
	return U
end

# ╔═╡ e1af316d-ffc3-4bf7-a599-6b9f5199e571
function policy_iteration(P, R)
	U = zeros(nof_states)
	pi = map(x->rand(1:nof_actions), 1:nof_states)
	unchanged = false
	
	while !unchanged
		U = policy_evaluation(pi, U, P, R)
		#println(U)
		unchanged = true
		for s in 1:nof_states
			act = argmax(map(a -> Qvalue(P, R, s, a, U), 1:nof_actions))
			if Qvalue(P, R, s, act, U) > Qvalue(P, R, s, pi[s], U)
				#println("Changing action at $s from $(pi[s]) to $act")
				pi[s] = act
				unchanged = false
				
			end
		end
	end
	return pi
end

# ╔═╡ 3e2c8567-8849-4e6d-b551-673b29aabc8d
policy = policy_iteration(P,R)

# ╔═╡ Cell order:
# ╟─98199c47-1b53-48ac-8e23-073740326b87
# ╠═3e2b34f6-d986-11eb-0d86-9b1ec2a4ec1e
# ╟─07020a9e-ba33-4cb4-aaa3-baa138e610f3
# ╠═d05e59bc-f62b-422a-8c6f-9381d54acecc
# ╠═37925a22-d774-4076-8ee7-a3f88f8b6f88
# ╠═ac9184d4-ebc2-4af2-b91c-ed106dbae337
# ╠═1101e7bf-0cfd-458b-a40b-7ff77cc10b50
# ╟─412d34c2-a77d-49ad-87cb-64e4c039b127
# ╟─e13c682f-a38d-48fc-89a0-232eb872dce6
# ╟─c54b6eae-ec48-4746-b733-86995ee10eb7
# ╟─e02659ec-18d9-44c0-a5e8-687f5b9c0775
# ╟─7b284aef-54d2-48de-93a7-b4fd0493a3a0
# ╟─603c82fc-4a16-48a5-8609-9968e9554baa
# ╟─b2c025ab-840e-4407-8fe9-4d93ff13f12a
# ╠═377a59f4-67bb-4e79-bc9a-6bf2814dd8d2
# ╠═bad25383-8359-4cc8-b6d4-5656acc62d4d
# ╠═80ac8292-9e62-4a0b-a639-504da3a33c34
# ╠═0cabddc5-74a9-4c56-af81-b11c4a09b4f2
# ╟─3d9b410c-e359-4c94-9822-72374f74752e
# ╠═614f978c-599a-4306-8e9c-2305fa17da43
# ╠═b7a410bb-0261-4ff3-bb08-8f99e17e2871
# ╠═1fa56533-541b-450e-b336-223c18df71e3
# ╠═e1af316d-ffc3-4bf7-a599-6b9f5199e571
# ╠═3e2c8567-8849-4e6d-b551-673b29aabc8d
